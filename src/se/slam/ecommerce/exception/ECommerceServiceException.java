package se.slam.ecommerce.exception;

public class ECommerceServiceException extends RuntimeException 
{
	private static final long serialVersionUID = 4784417854867030388L;
	
	public ECommerceServiceException() {
		super();
	}

	public ECommerceServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ECommerceServiceException(String message) {
		super(message);
	}

	public ECommerceServiceException(Throwable cause) {
		super(cause);
	}
}
