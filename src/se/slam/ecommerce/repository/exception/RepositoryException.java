package se.slam.ecommerce.repository.exception;

public class RepositoryException extends Exception
{
	private static final long serialVersionUID = -498431739389124123L;
	
	public RepositoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public RepositoryException (String message) {
		super(message);
	}

	public RepositoryException (Throwable cause) {
		super(cause);
	}
}
